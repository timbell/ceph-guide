# Introduction

This book provides User and Operator documentation for Ceph, especially as it pertains to the Ceph installations at CERN. [Ceph](http://ceph.com) is a distributed storage system offering low-level object storage with RADOS, network block devices with RBD, S3/SWIFT object storage ("REST-ful" object storage), and finally the POSIX-compatible network file system CephFS.

## Upstream Documentation and Releases

The Ceph project provides comprehensive upstream documentation for using and operating the software. Documentation is versioned along with the respective software releases:

| Ceph Release (Type) | Version | Documentation                         |
| ------------------- | ------- | ------------------------------------- |
| jewel (deprecated)  | 10.*    | http://docs.ceph.com/docs/jewel/      |
| luminous (LTS)      | 12.*    | http://docs.ceph.com/docs/luminous/   |
| mimic (LTS)         | 13.*    | http://docs.ceph.com/docs/mimic/      |
| master (unstable)   | n/a     | http://docs.ceph.com/docs/master/     |

It is generally advised to use only LTS versions in production.

## The Ceph Community

There are various ways to keep in touch with the community. The *ceph-users* mailing list is highly active and is the best place get help from expert users and developers. *ceph-devel* is where developers discuss upcoming features and releases. Ceph operators (and interested users) should follow these mailing lists to keep up with recent issues and developments. Details for joining and browsing list archives are [here](https://ceph.com/irc/).

Ceph issue tracking is hosted at http://tracker.ceph.com and the core softare repository is at https://github.com/ceph/ceph.

## This Book

The rest of this book aims to complement the upstream docs with local configurations and best-practises. The book is broken into two core chapters -- a User Guide and an Operators Guide. New Ceph operators are expected to have mastered the material presented in the User Guide.
