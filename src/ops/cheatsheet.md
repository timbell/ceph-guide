# mco

## Memory usage of all ceph-osd's in a cluster


```
@aiadm64 ~> mco shell run 'top -b -n1 | grep ceph-osd' -T ceph -F hostgroup=ceph/dwight/osd --dt=4 
```

## Ceph-mon disk usage

```
@aiadm64 ~> mco shell run 'du -hs /var/lib/ceph/mon' -T ceph -F hostgroup=ceph/beesly/mon --dt=4
```

# ceph

## Increase number of backfills

```
@cepherin ~> ulimit -n 10000; ceph tell osd.* injectargs -- --osd_max_backfills=2
```
