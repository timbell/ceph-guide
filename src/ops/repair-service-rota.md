# Pointers

There are several channels to watch during your Rota shift:

1. Emails to ceph-admins@cern.ch:
2. General informations on clusters (configurations, OSD types, HW, versions): [Instance Version Tracking ticket](https://its.cern.ch/jira/browse/CEPH-558)

# Taking notes

Each action you take should be noted down in the ticket, especially information such as OSD ID. 

# Keeping the Team Informed

If you have any questions or take any significant actions, keep ceph-admins informed.

# Common Procedures

* [scsi_blockdevice_driver_error_reported](#scsi_blockdevice_driver_error_reported)
   * [Draining a Failing OSD](#draining-a-failing-osd)
   * [Creating a new OSD](#creating-a-new-osd-on-a-replacement-disk)

## exception.scsi_blockdevice_driver_error_reported

### Draining a Failing OSD

This section describes how to prepare a disk to be physically removed.
The scripts needed for the replacement procedure may be found under `ceph-scripts/tools/ceph-disk-replacement/`.

*For failing OSDs in wigner cluster, contact ceph-admins*

*For failing SSDs contact ceph-admins*

0.  `watch ceph status` <- keep this open in a separate window.

1. Login to the machine with a failing drive and run `./drain-osd.sh --dev /dev/sdX` (the ticket should tell which drive is failing)
   You can also run the following command `ceph-volume lvm list /dev/sdX` to get more ceph-related information about the current drive (e.g. osd id).
   However note that this command might not work on cta/beesly cluster. 
   * For machines in /ceph/erin/osd/castor: You cannot run the script, ask ceph-admins (scripts will not let you run anything on these machines). 
   * If the output is of the following form: Take notes of the OSD id `<id>`
   ```
   ceph osd out osd.<id>
   ``` 
     
   * Else
     * Check for mispelled arguments
     * Ceph might be unhealthy or OSD is unsafe to stop, retry later or contact ceph-admins
     * Else if the script shows a broken output (especially missing `<id>`): Contact ceph-admins

2. Run `./drain-osd.sh --dev /dev/sdX | sh`

3. Once drained (can take a few hours), we now want to prepare the disk for replacement 
   * Run `./prepare-for-replacement.sh --dev /dev/sdX`
   * Continue if the output is of the following form and that the OSD id `<id>` displayed is consistent with what was given by the previous command: 
   ```
   systemctl stop ceph-osd@<id>
   umount /var/lib/ceph/osd/ceph-<id>
   ceph-volume lvm zap /dev/sdX --destroy
   ```
     * (note that the `--destroy` flag will be dropped in case of a FileStore OSD)
     
   * Else
     * If the script shows no output: Ceph is unhealthy or OSD is unsafe to stop, contact ceph-admins
     * Else if the script shows a broken output (especially missing `<id>`): Contact ceph-admins

3. Run `./prepare-for-replacement.sh --dev /dev/sdX | sh` to execute.

4. Now the disk is safe to be physically removed.

### Creating a new OSD (on a replacement disk)

When the broken disk has been replaced by a new one, we have to put it back into production.

0.  `watch ceph status` <- keep this open in a separate window.

1. Run `./recreate-osd.sh --dev /dev/sdX` and check that the output is according to the following:
  * On beesly cluster:
  ```
  ceph-volume lvm zap /dev/sdX
  ceph osd destroy <id> --yes-i-really-mean-it
  ceph-volume lvm create --osd-id <id> --data /dev/sdX --block.db /dev/sdY
  ```
  
  * On gabe cluster:
  ```
  ceph-volume lvm zap /dev/sdX
  ceph-volume lvm zap /dev/ceph-block-dbs-[0-6a-f]+/osd-block-db-[0-6a-z-]+
  ceph osd destroy <id> --yes-i-really-mean-it
  ceph-volume lvm create --osd-id <id> --data /dev/sdX --block.db ceph-block-dbs-[0-6a-f]+/osd-block-db-[0-6a-z-]+ 
  ```
 
  * On erin cluster: 
    * Regular case: 
    ```
    ceph-volume lvm zap /dev/sdX
    ceph osd destroy <id> --yes-i-really-mean-it
    ceph-volume lvm create --osd-id <id> --data /dev/sdX --block.db /dev/sdY
    ```
    
    * ceph/erin/castor/osd
      * Script cannot be run, contact ceph-admins.    
    
2. If the output is satisfactory, run `./recreate-osd.sh --dev /dev/sdX | sh`

   
