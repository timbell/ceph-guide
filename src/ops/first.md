# First, A Tutorial

This tutorial describes how to build a tiny Ceph cluster all on one VM. This can be useful while learning Ceph, but also for ops testing of major version upgrades, version (in)compatibilities, new features, etc...

What follows below is an abridged version of the walkthrough found [here](http://ceph.com/docs/master/start/quick-start-preflight/).

## Prerequisites

You should first create one new VM (m1.medium or larger) and attach a single Cinder volume of at least 500GB. Also disable selinux with setenforce permissive and make that permanent in /etc/sysconfig/selinux.

All commands below must be run as the `root` user.

## Installing *ceph-deploy*

First, we add the ceph *noarch* repository and install *ceph-deploy*. Edit `/etc/yum.repos.d/cephdeploy.repo` to have these contents:

```
[cephdeploy]
name=Ceph noarch packages
baseurl=http://linuxsoft.cern.ch/mirror/download.ceph.com/rpm-luminous/el7/noarch/
enabled=1
gpgcheck=1
type=rpm-md
gpgkey=https://download.ceph.com/keys/release.asc
```

Now install ceph-deploy:

```yum install ceph-deploy```

## Creating a *ceph-mon* daemon
Enter a new directory and create the new cluster with a local mon:

```
mkdir my-cluster && cd my-cluster
ceph-deploy new `hostname -s` 
```

Set the default pool replication size to 1: edit `ceph.conf` and add to the bottom:

```osd pool default size = 1```

Install the software and ceph-mon with

```
ceph-deploy install --release luminous --repo-url http://linuxsoft.cern.ch/mirror/download.ceph.com/rpm-luminous/el7/ --gpg-url http://cephmirror.cern.ch/keys/release.asc `hostname -s`
ceph-deploy mon create-initial
ceph-deploy admin `hostname -s`
ceph-deploy mgr create `hostname -s` 
```

Now the mon should be running. Confirm with `ceph -s`. It should show the mon running but HEALTH_ERR because the 192 PGs are still inactive -- there are no osds.
We now create the OSD process. First format and mount your data volume:
```
ceph-deploy gatherkeys `hostname -s`
ceph-deploy osd create `hostname -s` --data /dev/vdb
```
Now use `ceph -w` and you should see the PGs get created and eventually the cluster should be healthy, with all PGs active+clean.


## Testing, experimenting

Run ceph df to see your pools.

Check the contents of ```/var/lib/ceph/osd/ceph-0/``` to understand how an OSD stores data. Check ```/var/lib/ceph/mon``` to learn about the monitor's leveldb.

You can create a test pool and run some benchmark tests:
```
ceph osd pool create test 64
ceph -w to see the new pool get created
rados bench -p test 10 write -b 4096 -t 1 to test writing to the pool 
```

# CephFS
## Preparation

First, add a MDS using: 
```
ceph-deploy mds create `hostname -s`
```

From [1]: A Ceph filesystem requires at least two RADOS pools, one for data and one for metadata. When configuring these pools, you might consider:
- Using a higher replication level for the metadata pool, as any data loss in this pool can render the whole filesystem inaccessible.
- Using lower-latency storage such as SSDs for the metadata pool, as this will directly affect the observed latency of filesystem operations on clients.

To create the two pools with default settings for use with the filesystem, you might run the following commands:

```
ceph osd pool create cephfs_data <pg_num>
ceph osd pool create cephfs_metadata <pg_num>
```

(e.g. <pg_nm> equal 0). 

## Create the filesystem
Once the pools are created, you may enable the filesystem using the fs new command:

```
ceph fs new <fs_name> <metadata> <data>
```

For example:

```
ceph fs new cephfs cephfs_metadata cephfs_data
ceph fs ls
  name: cephfs, metadata pool: cephfs_metadata, data pools: [cephfs_data ]
```

Then mount the fs [2] : 
```
cp /etc/ceph/ceph.client.admin.keyring keyring
vi keyring #keep only the key
mkdir /mnt/cephfs_test
mount -t ceph <your IP>:6789:/ /mnt/cephfs_test/ -o name=admin,secretfile=keyring 
```

[1] http://docs.ceph.com/docs/master/cephfs/createfs/
[2] http://docs.ceph.com/docs/master/cephfs/kernel/
