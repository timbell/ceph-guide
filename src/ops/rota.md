# What to watch?

There are several channels to watch during your Rota shift:

1. Emails to ceph-admins@cern.ch:
   * "Ceph Health Warn" mails.
   * SNOW tickets from IT Repair Service.
   * Prometheus Alerts.

2. SNOW tickets assigned to Ceph Service:
   * Here is a link to the tickets needing to be taken: [Ceph Assigned](http://cern.ch/go/69MS)

3. Ceph Internal Mattermost [channel](https://mattermost.web.cern.ch/it-dep/channels/ceph-internal)

4. General informations on clusters (configurations, OSD types, HW, versions): [Instance Version Tracking ticket](https://its.cern.ch/jira/browse/CEPH-558)


# Taking notes

Each action you take should be noted down in a journal, which is to be linked or attached to the minutes of theCeph weekly meeting the following week. https://indico.cern.ch/category/9250/
Use HackMD, Notepad, ...

# Keeping the Team Informed

If you have any questions or take any significant actions, keep you colleagues informed in [Mattermost](https://mattermost.web.cern.ch/it-dep/channels/ceph-internal)

# Common Procedures

* [scsi_blockdevice_driver_error_reported](#scsi_blockdevice_driver_error_reported)
   * [Draining a Failing OSD](#draining-a-failing-osd)
   * [Creating a new OSD](#creating-a-new-osd-on-a-replacement-disk)
* [CephInconsistentPGs](#cephinconsistentpgs)
* [CephTargetDown](#cephtargetdown)

## exception.scsi_blockdevice_driver_error_reported

### Draining a Failing OSD

The IT Repair Service may ask ceph-admins to prepare a disk to be physically removed.
The scripts needed for the replacement procedure may be found under `ceph-scripts/tools/ceph-disk-replacement/`.

*For failing OSDs in wigner cluster, contact ceph-admins*

0.  `watch ceph status` <- keep this open in a separate window.

1. Login to the machine with a failing drive and run `./drain-osd.sh --dev /dev/sdX` (the ticket should tell which drive is failing)
   * For machines in /ceph/erin/osd/castor: You cannot run the script, ask ceph-admins. 
   * If the output is of the following form: Take notes of the OSD id `<id>`
   ```
   ceph osd out osd.<id>
   ``` 
     
   * Else
     * If the script shows no output: Ceph is unhealthy or OSD is unsafe to stop, contact ceph-admins
     * Else if the script shows a broken output (especially missing `<id>`): Contact ceph-admins

2. Run `./drain-osd.sh --dev /dev/sdX | sh`

3. Once drained (can take a few hours), we now want to prepare the disk for replacement 
   * Run `./prepare-for-replacement.sh --dev /dev/sdX`
   * Continue if the output is of the following form and that the OSD id `<id>` displayed is consistent with what was given by the previous command: 
   ```
   systemctl stop ceph-osd@<id>
   umount /var/lib/ceph/osd/ceph-<id>
   ceph-volume lvm zap /dev/sdX --destroy
   ```
     * (note that the `--destroy` flag will be dropped in case of a FileStore OSD)
     
   * Else
     * If the script shows no output: Ceph is unhealthy or OSD is unsafe to stop, contact ceph-admins
     * Else if the script shows a broken output (especially missing `<id>`): Contact ceph-admins

3. Run `./prepare-for-replacement.sh --dev /dev/sdX | sh` to execute.

4. Now the disk is safe to be physically removed.
   * Notify the repair team in the ticket

### Creating a new OSD (on a replacement disk)

When the IT Repair Service has replaced the broken disk with a new one, we have to format that disk with BlueStore to add it back to the cluster:

0.  `watch ceph status` <- keep this open in a separate window.

1. Identify the osd id <id> to use on this OSD:
   * Check your notes from the drain procedure above.
   * Cross-check with `ceph osd tree down` <-- look for the down osd on this host, should match your notes.

2. Run `./recreate-osd.sh --dev /dev/sdX` and check that the output is according to the following:
  * On beesly cluster:
  ```
  ceph-volume lvm zap /dev/sdX
  ceph osd destroy <id> --yes-i-really-mean-it
  ceph-volume lvm create --osd-id <id> --data /dev/sdX --block.db /dev/sdY
  ```
  
  * On gabe cluster:
  ```
  ceph-volume lvm zap /dev/sdX
  ceph-volume lvm zap /dev/ceph-block-dbs-[0-6a-f]+/osd-block-db-[0-6a-z-]+
  ceph osd destroy <id> --yes-i-really-mean-it
  ceph-volume lvm create --osd-id <id> --data /dev/sdX --block.db ceph-block-dbs-[0-6a-f]+/osd-block-db-[0-6a-z-]+ 
  ```
 
  * On erin cluster: 
    * Regular case: 
    ```
    ceph-volume lvm zap /dev/sdX
    ceph osd destroy <id> --yes-i-really-mean-it
    ceph-volume lvm create --osd-id <id> --data /dev/sdX --block.db /dev/sdY
    ```
    
    * ceph/erin/castor/osd
      * Script cannot be run, contact ceph-admins.    
    
3. If the output is satisfactory, run `./recreate-osd.sh --dev /dev/sdX | sh`

See [OSD Replacement](osd_replacement.md) for many more details.

## CephInconsistentPGs

Familiarize yourself with the [Upstream documentation](http://docs.ceph.com/docs/luminous/rados/troubleshooting/troubleshooting-pg/#pgs-inconsistent)

Check ceph.log on a ceph/*/mon machine to find the original "cluster [ERR]" line.

The inconsistent PGs generally come in two types:
1. deep-scrub: stat mismatch, solution is to [repair the PG](#repairing-a-pg)
   * Here is an example on `ceph\flax`:
```
2019-02-17 16:23:05.393557 osd.60 osd.60 128.142.161.220:6831/3872729 56 : cluster [ERR] 1.85 deep-scrub : stat mismatch, got 149749/149749 objects, 0/0 clones, 149749/149749 dirty, 0/0 omap, 0/0 pinned, 0/0 hit_set_archive, 0/0 whiteouts, 135303283738/135303284584 bytes, 0/0 hit_set_archive bytes.
2019-02-17 16:23:05.393566 osd.60 osd.60 128.142.161.220:6831/3872729 57 : cluster [ERR] 1.85 deep-scrub 1 errors
```
2. candidate had a read error, solution follows below.
  * Notice that the doc says **If read_error is listed in the errors attribute of a shard, the inconsistency is likely due to disk errors. You might want to check your disk used by that OSD.** This is indeed the most common scenario.

### 

### Handle a failing disk
In this case, a failing disk returns bogus data during deep scrubbing, and ceph will notice that the replicas are not all consistent with each other. The correct procedure is therefore to remove the failing disk from the cluster, let the PGs backfill, then finally to deep-scrub the inconsistent PG once again.

Here is an example on `ceph/erin` cluster, where the monitoring has told us that PG `64.657c` has an inconsistent PG:

```
[09:16][root@cepherin0 (production:ceph/erin/mon*1) ~] grep shard /var/log/ceph/ceph.log
2017-04-12 06:34:26.763000 osd.508 128.142.25.116:6924/4070422 4602 : cluster [ERR] 64.657c shard 187:
soid 64:3ea78883:::1568573986@castorns.27153415189.0000000000000034:head candidate had a read error
```

A *shard* in this case refers to which OSD has the inconsistent object replica, in this case it's the "osd.187".

### Where is osd.187?

```
[09:16][root@cepherin0 (production:ceph/erin/mon*1) ~]# ceph osd find 187
{
   "osd": 187,
   "ip": "128.142.25.106:6820\/530456",
   "crush_location": {
       "host": "p05972678k94093",
       "rack": "EC06",
       "room": "0513-R-0050",
       "root": "default",
       "row": "EC"
   }
}
```

On the `p05972678k94093` host we first need to find out which /dev/sd* device hosts that osd.187:

On FileStore OSDs, we can check the mounted disks directly:

```
[09:16][root@p05972678k94093 (production:ceph/erin/osd*29) ~]# mount | grep 187
/dev/sdm1 on /var/lib/ceph/osd/ceph-187 type xfs (rw,noatime,seclabel,attr2,inode64,logbufs=8,logbsize=256k,noquota)
```

On BlueStore OSDs we need to check with `ceph-volume lvm list` or `lvs`:

```
[14:38][root@p05972678e32155 (production:ceph/erin/osd*30) ~]# lvs -o +devices,tags | grep 187
  osd-block-... ceph-... -wi-ao---- <5.46t        /dev/sdm(0) ....,ceph.osd_id=187,....
```

So we know the failed drive is `/dev/sdm`, now we can check for disk Medium errors:

```
[09:16][root@p05972678k94093 (production:ceph/erin/osd*29) ~]# grep sdm /var/log/messages
[Wed Apr 12 12:27:59 2017] sd 1:0:10:0: [sdm] CDB: Read(16) 88 00 00 00 00 00 05 67 07 20 00 00 04 00 00 00
[Wed Apr 12 12:27:59 2017] blk_update_request: critical medium error, dev sdm, sector 90638112
[Wed Apr 12 12:28:02 2017] sd 1:0:10:0: [sdm] FAILED Result: hostbyte=DID_OK driverbyte=DRIVER_SENSE
[Wed Apr 12 12:28:02 2017] sd 1:0:10:0: [sdm] Sense Key : Medium Error [current]
[Wed Apr 12 12:28:02 2017] sd 1:0:10:0: [sdm] Add. Sense: Unrecovered read error
[Wed Apr 12 12:28:02 2017] sd 1:0:10:0: [sdm] CDB: Read(16) 88 00 00 00 00 00 05 67 07 20 00 00 00 08 00 00
[Wed Apr 12 12:28:02 2017] blk_update_request: critical medium error, dev sdm, sector 90638112
```

In this case, the disk is clearly failing. 

Now check if that osd is safe to stop?

```
[14:41][root@p05972678k94093 (production:ceph/erin/osd*30) ~]# ceph osd ok-to-stop osd.187
OSD(s) 187 are ok to stop without reducing availability, provided there are no other concurrent failures or interventions. 182 PGs are likely to be degraded (but remain available) as a result.
```

Since it is OK, we stop the osd, umount it, and mark it out.

```
[09:17][root@p05972678k94093 (production:ceph/erin/osd*30) ~]# systemctl stop ceph-osd@187.service
[09:17][root@p05972678k94093 (production:ceph/erin/osd*30) ~]# umount /var/lib/ceph/osd/ceph-187
[09:17][root@p05972678k94093 (production:ceph/erin/osd*29) ~]# ceph osd out 187
marked out osd.187.
```

`ceph status` should now show the PG is in a state like this:

```
             1     active+undersized+degraded+remapped+inconsistent+backfilling
```

It can take a few 10s of minutes to backfill the degraded PG.
Once the inconsistent PG is no longer "undersized" or "degraded", start a deep-scrub at that PG:

```
ceph pg deep-scrub 64.657c
```

Now check `ceph status`... you should see the deep-scrub started already on the inconsistent PG. If not, check the next section.

### Repairing a PG

We occasionally need to repair (or deep-scrub) a PG, but the usual command `ceph pg repair <pgid>` (or `ceph pg deep-scrub <pgid>`) doesn't start immediately.

Repair is a type of scrub, and each OSD scrubs at most `osd_max_scrubs`; this is limited per OSD, and it is normal to be set to `1`.
If the repair/deep-scrub is not starting, this means that at least one of the OSDs hosting your PG is already scrubbing.

The solution is therefore to increase `osd_max_scrubs` and only let the forced scrub/repair start.

Here is a recipe to do this:

```
$ ulimit -n 10000
$ ceph osd set noscrub
$ ceph osd set nodeep-scrub
$ ceph tell osd.* injectargs -- --osd_max_scrubs=2 --osd_scrub_during_recovery=1
$ ceph pg repair <pgid>

# confirm that <pgid> started deep-scrubbing and/or repairing, e.g:
#     ceph -s should show: "1     active+clean+scrubbing+deep+inconsistent+repair"

$ ceph tell osd.* injectargs -- --osd_max_scrubs=1 --osd_scrub_during_recovery=0
$ ceph osd unset nodeep-scrub
$ ceph osd unset noscrub
```

## CephTargetDown

This is an special alert raised by prometheus. This indicates that for whatever reason a `target` node is not exposing
its metrics anymore or prometheus server is not able to pull them. This does not imply that the node is offline, just 
that the node endpoint is down for prometheus. 

To handle this tickets first we need to identify which is the affected target. This information should be in the ticket 
body.

```angular2html
The following Alerts are in Firing Status:
------------------------------------------------
Target cephpolbo-mon-0.cern.ch:9100 is down
Target cephpolbo-mon-2.cern.ch:9100 is down

Alert Details:
------------------------------------------------
Alertname: TargetDown
Cluster: polbo
Job: node
Monitor: cern
Replica: A
Severity: warning
``` 

After, we can go to the `target` section in prometheus's [dashboard](https://cephprom.cern.ch/targets) and cross-check
the affected node. There you can find more information about the reason of being down. 

This could be caused by the following reasons:
- A node is offline or it's being restarted. Follow the normal procedures for understanding why the node is not online
    (ping, ssh, console access, SNOW ticket search...). Once the node is back, the target should be marked as UP again 
    automatically.
- If a new target was added recently, possibly there are mistakes in the target definition or some conectivity problems
like the port being blocked.
    - Review the target configuration in `it-hostgroup-ceph/data/hostgroup/ceph/prometheus.yaml` and refer to the 
    monitoring [guide](monitoring.md).
    - Make sure that the firewall configuration allows prometheus to scrape the data through the specified port.
- In ceph, the daemons that expose the metrics are the `mgr`. Sometimes, could happen that the mgr hangs and then stop
exposing the metrics.
    - Check the `mgr` status and eventually restart it. Don't forget to collect information about the state in what you 
    found it for further analysis. If all went well, after 30 seconds, the target should be `UP` again in prometheus
    dashboard. For double-check you can click in the `endpoint` url of the node and see if the metrics are now shown.  
    
