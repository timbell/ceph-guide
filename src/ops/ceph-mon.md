# Operating the Ceph Monitors (ceph-mon)

## Adding ceph-mon daemons (VM, jewel/luminous)

Upstream documentation here: http://docs.ceph.com/docs/master/rados/operations/add-or-rm-mons/

Normally we create ceph-mon's as VMs in the ceph/{hg_name}/mon hostgroup.

Example: Adding a monitor to the **ceph/test** cluster:

 - First, source the IT Ceph Storage Service environment on aiadm: [link](openstack.md)
 - Then create a virtual machine with the following parameters:
  - main-user/responsible: ceph-admins (the user of the VM)
  - VM Flavor: m2.2xlarge (monitors must withstand heavy loads)
  - OS: Centos7 (the preferred OS used in CERN applications)
  - Hostgroup: ceph/test/mon (Naming convention for puppet configuration)
  - VM name: cephtest-mon- (We use prefix to generate an id)
  - Availability zone: usually cern-geneva-[a/b/c]

Example command: (It will create a VM with the above parameters)
```sh
$ ai-bs --landb-mainuser ceph-admins --landb-responsible ceph-admins --nova-flavor m2.2xlarge
--cc7 -g ceph/test/mon --prefix cephtest-mon-  --nova-availabilityzone cern-geneva-a
--nova-sshkey {your_openstack_key}
```

This command will create a VM named `cephtest-mon-XXXXXXXXXX` in the ceph/test/mon
hostgroup. Puppet will take care of the initialization of the machine

When you deploy a monitor server, you have to choose an availability zone.
We tend to use different availability zones to avoid a [single point of failure](https://en.wikipedia.org/wiki/Single_point_of_failure).

---

[//]: # (TODO: We may need a bit more info about roger)

  - Set the `appstate` and `app_alarmed` parameters if necessary

Example: Get the roger data for the VM `cephtest-mon-d8788e3256`

```sh
$ roger show cephtest-mon-d8788e3256
```

The output should be something similar to this:

```json
[
    {
        "app_alarmed": false,
        "appstate": "build",
        "expires": "",
        "hostname": "cephtest-mon-d8788e3256.cern.ch",
        "hw_alarmed": true,
        "message": "",
        "nc_alarmed": true,
        "os_alarmed": true,
        "update_time": "1506418702",
        "update_time_str": "Tue Sep 26 11:38:22 2017",
        "updated_by": "tmourati",
        "updated_by_puppet": false
    }
]
```

You need to set the machine's state to "production", so it can be used in production.

The following command will set the target VM to production state:

```sh
$ roger update --appstate production --all_alarms=true cephtest-mon-XXXXXXXXXX
```

Now the `roger show {host}` should show something like this:

```json
[
    {
        "app_alarmed": true,
        "appstate": "production",
        "..."
    }
]
```

We now let puppet configure the machine. This task will take an adequate amount of
time, as it needs about two configuration cycles to apply the desired changes. After
the second cycle you can SSH (as root) to the machine to check if everything is ok.

For example you can check the cluster's status with `$ ceph -s`

You should see the current host in the monitor quorum.


## Removing a ceph-mon daemon (jewel)

Upstream documentation here: http://docs.ceph.com/docs/master/rados/operations/add-or-rm-mons/

### Prerequisites

  1. The cluster must be in `HEALTH_OK` state, i.e. the monitor must be in a a healthy quorum.
  2. You should have a replacement for the current monitor already in the quorum.
And there should be enough monitors so that the cluster can be healthy after one monitor is removed.
Normally this means that we should have about 4 monitors in the quorum before starting.

### Procedure

  1. Move this machine to a spare hostgroup: `$ ai-foreman updatehost -c ceph/spare {hostname}`
  2. Run puppet once: `$ puppet agent -t`
  3. Stop puppet: `$ puppet agent --disable 'decommissioning mon'`
  4. **(If needed)** remove the DNS alias from this machine:
For physical machines, visit http://network.cern.ch → "Update Information".
For a VM monitor, you can remove the alias from the `landb-alias` property. See
[Cloud Docs](https://clouddocs.web.cern.ch/clouddocs/using_openstack/properties.html)
  5. Stop the monitor: `$ systemctl stop ceph.target`. You should now get a `HEALTH_WARN` status by running `$ ceph -s`, for example `1 mons down, quorum 1,2,3,4,5`.
  6. Remove the monitor's configuration, data and secrets with:
```sh
$ rm /etc/ceph/*
$ rm /var/lib/ceph/tmp/keyring.mon.*
$ rm -rf /var/lib/ceph/mon/mon.*
```
  7. Remove the monitor from the ceph cluster:
```sh
$ ceph mon rm p01001532077488
removing mon.p01001532077488 at 128.142.36.227:6790/0, there will be 5 monitors
```
You should now have a `HEALTH_OK` status after the monitor removal.

  8. **(If physical)** Reinstall the server in the `ceph/spare` hostgroup:
```sh
aiadm> ai-installhost p01001532077488
...
1/1 machine(s) ready to be installed
Please reboot the host(s) to start the installation:
ai-remote-power-control cycle p01001532077488.cern.ch
aiadm> ai-remote-power-control cycle p01001532077488.cern.ch
```
  Now the physical machine is installed in the `ceph/spare` hostgroup.

  9. **(If virtual)** Kill the vm with: `$ ai-kill-vm {hostname}`
