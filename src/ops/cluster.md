# Creating a CEPH cluster

### Table of Contents

  * [Introduction](#prerequisites)
  * [Puppet configuration](#creating-a-configuration-for-your-new-cluster)
  * [Creating monitor hosts](#creating-your-first-monitor-node)
  * [Creating osd hosts](#creating-osd-hosts)
  * [Creating mds hosts for CephFS](#creating-metadata-servers-cephfs)
  * [Creating a Rados Gateway](#creating-rgw-hosts)

Follow the below instructions to create a new CEPH cluster in CERN

## Prerequisites

 * Access to aiadmin.cern.ch
 * Proper GIT configuration
 * Member of ceph administration e-groups
 * Openstack environment configured, [link](openstack.md)

## Introduction - Hostgroups

First, we have to create the **hostgroups** in which we want to build our cluster in.

The hostgroups provide a layer of abstraction for configuring automatically a cluster
using **Puppet**. The first group called **ceph**, ensures that each machine in
this hostgroup has ceph installed, configured and running. The second group,
called first sub-hostgroup, ensures that each machine will communicate with machines
in the same sub-hostgroup forming a cluster. These machines will have specific
configuration defined later in this guide. The second sub-hostgroup ensures that
each machine will act as its corresponding role in the cluster.

For example we first create our cluster's hostgroup with its name that is provided
by your task.

```
[user@aiadm]$ ai-foreman addhostgroup ceph/{hg_name}
```

As each cluster has its own features, the 2 basic sub-hostgroups for a ceph cluster
are the **mon** and **osd**.

```
[user@aiadm]$ ai-foreman addhostgroup ceph/{hg_name}/mon
[user@aiadm]$ ai-foreman addhostgroup ceph/{hg_name}/osd
```

These sub-hostgroups will contain the monitors and the osd hosts.

If the cluster has to use CephFS and/or Rados gateway we need to create the appropriate
sub-hostgroups.

```
[user@aiadm]$ ai-foreman addhostgroup ceph/{hg_name}/mds      #for CephFS
[user@aiadm]$ ai-foreman addhostgroup ceph/{hg_name}/radosgw  #for the rados gateway
```

## Creating a configuration for your new cluster

Go to gitlab.cern.ch and search for `it-puppet-hostgroup-ceph`. This repository contains the configuration for all the machines under the ceph hostgroup.

Clone the repository, create a new branch, and go to `it-puppet-hostgroup-ceph/code/manifests`. From there you will create the `{hg_name}.pp` file and `{hg_name}` folder.

The `{hg_name}.pp` should contain the following code: (replace `{hg_name}` with the cluster's name)
```
class hg_ceph::{hg_name} {
  include ::hg_ceph::include::base

  osrepos::kojitag { 'ceph-luminous':
      available_major_versions => [7],
      description              => 'Ceph Luminous binaries from CERN Koji',
      priority                 => 9,
  }
}
```

This will load the basic configuration for ceph on each machine and it will install the latest available luminous version.
The `{hg_name}` folder will contain the __*.pp__ files for the appropriate 2nd sub-hostgroups.

The files under your cluster's folder will have the following basic format:

File `{role}.pp`:
```
class hg_ceph::{hg_name}::{role} {

  include ::hg_ceph::classes::{role}

}
```
The include will use a configuration template located in `it-puppet-hostgroup-ceph/code/manifests/classes`

The roles are: **mon, mgr, osd, mds and radosgw**. Because the monitors in luminous version are coupled with mgrs we will have to add them together e.g.:
```
class hg_ceph::{hg_name}::mon {

  include ::hg_ceph::classes::mon
  include ::hg_ceph::classes::mgr

}
```

The following code will configure machines in "_ceph/{hg_name}/mon_" to act as monitors and mgrs together.

After you are done with creating the needed files for your task. Your "_code/manifests_" path should look like this:
```
#Using kermit as {hg_name}

kermit.pp
kermit/mon.pp
kermit/osd.pp
#Optional, only if requested by task
kermit/mds.pp
kermit/radosgw.pp
```

Git add the following files, commit and push your branch. **BEFORE** you push, do a `git pull --rebase origin master` to avoid any
conflicts with your request. The command line will provide a link to submit a merge request.

@dvanders is currently the administrator of the repo, so you should assign him the task to look your request and eventually merge it.

## Creating your first monitor node

Follow the instructions to create a new monitor [here](ceph-mon.md).

### With TBag authentication

Once we are able to login to the node, we will need to create the keys to be able
to bootstrap new nodes to the cluster.

Mons and Mgrs go together in each node, so we need keys for both.

```
[root@ceph{hg_name}-mon-...]# ceph-authtool --create-keyring /tmp/keyring.mon --gen-key -n mon. --cap mon 'allow *'
[root@ceph{hg_name}-mon-...]# ceph auth get-or-create-key client.bootstrap-mgr mon 'allow profile bootstrap-mgr'
[root@ceph{hg_name}-mon-...]# ceph auth get client.bootstrap-mgr > /tmp/keyring.bootstrap-mgr
```

We will need to repeat this procedure for the osd and mds and rgw:
```
[root@ceph{hg_name}-mon-...]# ceph auth get-or-create-key client.bootstrap-osd mon 'allow profile bootstrap-osd'
[root@ceph{hg_name}-mon-...]# ceph auth get client.bootstrap-osd > /tmp/keyring.bootstrap-osd
# Optional, only if the cluster uses CephFS
[root@ceph{hg_name}-mon-...]# ceph auth get-or-create-key client.bootstrap-mds mon 'allow profile bootstrap-mds'
[root@ceph{hg_name}-mon-...]# ceph auth get client.bootstrap-mds > /tmp/keyring.bootstrap-mds
# Optional, only if the cluster uses a Rados Gateway
[root@ceph{hg_name}-mon-...]# ceph auth get-or-create client.radosgw osd 'allow rwx' mon 'allow rw' -o /tmp/keyring.radosgw
```

Login to aiadm, copy the keys from the monitor host and use them with tbag.

Make sure you don't have any excess keys in the `/tmp` folder (4 max, mon/osd/mds/rgw).
We don't need to provide the specific subgroup for each key, because that will cause confusion,
"*ceph/{hg_name}*" is enough.

```
[user@aiadm]$ cd /afs/cern.ch/project/ceph/private/tbag/{hg_name}
[user@aiadm]$ scp {mon_host}:/tmp/keyring.* .
[user@aiadm]$ tbag set --hg ceph/{hg_name}/mon keyring.mon --file keyring.mon
[user@aiadm]$ tbag set --hg ceph/{hg_name} keyring.bootstrap-mgr --file keyring.bootstrap-mgr
# Optional
[user@aiadm]$ tbag set --hg ceph/{hg_name} keyring.bootstrap-mds --file keyring.bootstrap-mds
[user@aiadm]$ tbag set --hg ceph/{hg_name} keyring.radosgw --file keyring.radosgw
```

Now we create the other monitors using the same procedure as the first one (`ai-bs-vm`).

## Creating osd hosts

**TBD**

## Creating metadata servers (CephFS)

**TBD**

## Creating rgw hosts

**TBD**
